# Home Assignments
Erkka N

- H1.1 Smallest and largest
- H1.2 Number between
- H1.3 Square root
- H1.4 Sum

<br>

- H2.1 Bulls and Cows
- H2.2 Deck of cards

<br>

- H3 & H4 Mars rover
    - H3.1 Mars rover object
    - H3.2 Mars rover class
    - H3.3 Two rovers
    - H3.4 2D map
    - H3.5 Collision detection
    - H3.6 Obstacles
    - H4.1 Automatic movement
    - H4.2 Two rovers out of sync
    - H4.3 Visualization
    - H4.X Remote Controlled Rover (personal extra)

<br>

- H5 API for the Library
    - H5.1 Create a new user with POST
    - H5.2 GET number of borrowed books
    - H5.3 GET a list of borrowed books
    - H5.4 Borrow a book with PATCH
    - H5.5 Return a book with PATCH
    - H5.6 Update account with PATCH

<br>

- H6 Note-taking CRUD application (React)
    - H6.1 Create
    - H6.2 Read
    - H6.3 Delete
    - H6.4 Update
    - H6.5 CSS

<br>

- H7 Memory game (React)

<br>

## Install dependencies with npm and run with globally installed TypeScript
```
git clone <url>
cd <repo>
npm install
ts-node app.ts
```

## Initialise
```
npm init
tsc --init
npm install @types/node
npm install readline-sync
npm install @types/readline-sync
```

## Git
```
git init / git clone <repo>
echo "node_modules/" >> .gitignore
git branch <newbranch>
git checkout branch
git merge master
git add .
git commit -m "Commit message"
git checkout master
git merge branch
git pull
git push
```
