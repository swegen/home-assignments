// H4.3 Visualization
// Erkka N
// I added random obstacle and rover positions and directions to the exercise
// to have more variations in the movement.

{ // blank namespace to prevent name clashes

	type RoverRange = 0|1|2|3|4|5|6|7|8|9;

	interface IPosition {
		x: RoverRange;
		y: RoverRange;
	}
	
	interface IOposition extends IPosition {
		found: Boolean
	}

	type Direction = 'NORTH' | 'EAST' | 'SOUTH' | 'WEST';
	
	class NamedRover {
		public position: IPosition;
		private direction: Direction;
		public log: IPosition[];
		public name: string;
		public lastMoveSuccessful: Boolean;
	
	
		constructor(name: string, position: IPosition) {
			this.name = name;
			this.position = position;
			const directions = ['NORTH', 'EAST', 'SOUTH', 'WEST'];
			this.direction = directions[Math.floor(Math.random() * directions.length)] as Direction; // random starting direction
			this.lastMoveSuccessful = true;
			this.log = [{...this.position}]; // make a copy of current position object
		}
	
	
		// returns true if the move was successful, else returns false
		moveForward(): Boolean {
			const newPosition: IPosition = {...this.position};

			switch (this.direction) {
				case 'NORTH':
					--newPosition.y;
					break;
				case 'EAST':
					++newPosition.x;
					break;
				case 'SOUTH':
					++newPosition.y;
					break;
				case 'WEST':
					--newPosition.x;
					break;
			}

			// check to stay on the grid
			if (newPosition.x < 0 || newPosition.x > 9 || newPosition.y < 0 || newPosition.y > 9) {
				console.log(`${this.name} FULL STOP! Cannot move outside communications range to ${this.direction} from ${this.position.x},${this.position.y}.`);
				return false;
			}

			// obstacle detection
			const obstacle = obstacles.find(obstacle => obstacle.x === newPosition.x && obstacle.y === newPosition.y);
			if (obstacle !== undefined) {
				obstacle.found = true; // mark obstacle as found, so it will be drawn
				console.log(`${this.name} FULL STOP! Our sensors are detecting a Martian ahead in ${this.direction} at ${newPosition.x},${newPosition.y}.`);
				return false;
			}

			// detect collision with another rover
			for (const rover in rovers) {
				if (rovers[rover].position.x === newPosition.x && rovers[rover].position.y === newPosition.y) {
					console.log(`${this.name} FULL STOP! We don't want to hit ${rovers[rover].name} at ${rovers[rover].position.x},${rovers[rover].position.y}.`);
					return false;
				}
			}

			this.position = {...newPosition}; // update position
			this.log.push({...this.position});
			console.log(`${this.name} moving towards ${this.direction} to coordinates ${this.position.x},${this.position.y}.`);
			return true;
		} // moveForward()


		turnLeft() {
			switch (this.direction) {
				case 'NORTH':
					this.direction = 'WEST';
					break;
				case 'EAST':
					this.direction = 'NORTH';
					break;
				case 'SOUTH':
					this.direction = 'EAST';
					break;
				case 'WEST':
					this.direction = 'SOUTH';
					break;
			}
			console.log(`${this.name} turning left to face ${this.direction}.`);
		}
	
		turnRight() {
			switch (this.direction) {
				case 'NORTH':
					this.direction = 'EAST';
					break;
				case 'EAST':
					this.direction = 'SOUTH';
					break;
				case 'SOUTH':
					this.direction = 'WEST';
					break;
				case 'WEST':
					this.direction = 'NORTH';
					break;
			}
			console.log(`${this.name} turning right to face ${this.direction}.`);
		}
	
		printPosition() {
			console.log(`${this.name} currently positioned in ${this.position.x},${this.position.y}.`)
		}
	
		// prints travelled path for all rovers in the vicinity
		printLog() {
			console.log(`Travel Log:`);

			for (let y=0; y<=9; ++y) {
				let line: string = '';
				for (let x=0; x<=9; ++x) {

					let posDrawn: Boolean = false;

					const obstacle = obstacles.some(obstacle => obstacle.x === x && obstacle.y === y && obstacle.found === true);
					if (obstacle === true) { // draw possible obstacle
						line += '@ '; // Martian
						posDrawn = true;
					}
					else
					for (const rover in rovers) { // draw rover position
						const r = rovers[rover];
						r.log.forEach((pos, i) => {
							if (pos.x === x && pos.y === y) {
								if (!posDrawn) {
									const char = (i===0 ? r.name[0].toUpperCase() : r.name[0].toLowerCase()); // starting position as uppercase letter
									line += `${char} `;
									posDrawn = true;
								}
							}
						});			
					}

					if (!posDrawn) line += '- '; // draw empty grid
				}
				console.log(line);
			}

		} // printLog()
	} // Rover class


	// returns an array of n items with unique x,y positions of coordinates between 0 and 9
	// I first tried using a new Set(), but it couldn't differentiate arrays or objects
	function generateUniquePositions(n: number): IPosition[] {
		const ran10 = () => Math.floor(Math.random()*10) as RoverRange;
		const positions: any = []; // how to type empty collection as IPosition[] ?
		while (positions.length < n) {
			const newPos: IPosition = {x:ran10(), y:ran10()};
			if (positions.some((pos: IPosition) => pos.x === newPos.x && pos.y === newPos.y) === false)
				positions.push(newPos);
		}
		return positions;
	}


	interface IRovers {
		[x: string]: NamedRover; // allow any number of NamedRover objects inside
	}

	// randomize rover and obstacle positions
	const positions = generateUniquePositions(5);
	const rover1 = new NamedRover('Sojourner', positions.pop() as IPosition);
	const rover2 = new NamedRover('Opportunity', positions.pop() as IPosition);
	const rovers: IRovers = {Sojourner:rover1, Opportunity:rover2};
	const obstacles: IOposition[] = [ // list of obstacle positions
		{...positions.pop() as IPosition, found:false},
		{...positions.pop() as IPosition, found:false},
		{...positions.pop() as IPosition, found:false},
	];


	function printMap() {
		let mapLines = 'Map inside the communications range of the Mars lander\n';

		for (let y=0; y<=9; ++y) {
			let line: string = '';
			for (let x=0; x<=9; ++x) {

				let posDrawn: Boolean = false;

				const obstacle = obstacles.some(obstacle => obstacle.x === x && obstacle.y === y && obstacle.found === true);
				if (obstacle === true) { // draw found obstacles
					line += '@ '; // Martian
					posDrawn = true;
				}
				else
				for (const rover in rovers) { // draw rover position with its starting letter
					const r = rovers[rover];
					if (r.position.x === x && r.position.y === y) {
						if (!posDrawn) {
							line += `${r.name[0].toUpperCase()} `;
							posDrawn = true;
						}
					}
				}

				if (!posDrawn) line += '- '; // draw empty grid
			}
			mapLines += `${line}\n`;
		}

		console.clear();
		console.log(mapLines);
	}


	// delay the step function execution
	function setDelay(rover: NamedRover) {
		const delay = Math.floor(Math.random()*1001+1000); // somewhere between 1 and 2 seconds (try 100ms for more speed)
		setTimeout(step, delay, rover);
	}

	// start taking steps with a rover
	function step(rover: NamedRover) {
		printMap();
		if (rover.lastMoveSuccessful === true) {
			rover.lastMoveSuccessful = rover.moveForward();
		}
		else { // random turn direction
			const turnCommands = ['left', 'right'];
			const turnCommand = turnCommands[Math.floor(Math.random() * turnCommands.length)];
			turnCommand === 'left' ? rover.turnLeft() : rover.turnRight();
			rover.lastMoveSuccessful = true;
		}
		setDelay(rover);
	}

	printMap();
	// random initial delays
	setDelay(rover1);
	setDelay(rover2);


} // namespace
