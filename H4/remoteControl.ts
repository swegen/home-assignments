// H4.X Remote Controlled Rover (extra addition for fun)
// Erkka N
// Now you can control one rover yourself using the arrow keys,
// while the other two rovers continue to do their thing.
// There are three Martians hiding in the vicinity of the Mars lander.

{ // blank namespace to prevent name clashes

	type RoverRange = 0|1|2|3|4|5|6|7|8|9;

	interface IPosition {
		x: RoverRange;
		y: RoverRange;
	}
	
	interface IOposition extends IPosition {
		found: boolean
	}

	type Direction = 'NORTH' | 'EAST' | 'SOUTH' | 'WEST';

	class NamedRover {
		public position: IPosition;
		private direction: Direction;
		public log: IPosition[];
		public name: string;
		public glyph: string;
		public lastMoveSuccessful: boolean;
		private userControlled: boolean;


		constructor(name: string, position: IPosition, isUserControlled?: boolean) {
			this.name = name;
			this.glyph = name[0].toUpperCase();
			this.userControlled = (isUserControlled === true) ? true : false;
			this.position = position;
			const directions = ['NORTH', 'EAST', 'SOUTH', 'WEST'];
			this.direction = directions[Math.floor(Math.random() * directions.length)] as Direction; // random starting direction
			if (this.userControlled === true) this.setGlyph();
			this.lastMoveSuccessful = true;
			this.log = [{...this.position}]; // make a copy of current position object
		}


		moveBackward() {
			const newPosition: IPosition = {...this.position};
			switch (this.direction) {
				case 'NORTH':
					++newPosition.y;
					break;
				case 'EAST':
					--newPosition.x;
					break;
				case 'SOUTH':
					--newPosition.y;
					break;
				case 'WEST':
					++newPosition.x;
					break;
			}
			this.moveForward(newPosition);
		}


		// returns true if the move was successful, else returns false
		moveForward(reverse?: IPosition): boolean {
			const newPosition: IPosition = (reverse !== undefined) ? reverse : {...this.position};

			if (reverse === undefined) {
				switch (this.direction) {
					case 'NORTH':
						--newPosition.y;
						break;
					case 'EAST':
						++newPosition.x;
						break;
					case 'SOUTH':
						++newPosition.y;
						break;
					case 'WEST':
						--newPosition.x;
						break;
				}
			}

			// check to stay on the grid
			if (newPosition.x < 0 || newPosition.x > 9 || newPosition.y < 0 || newPosition.y > 9) {
				lastMessage = `${this.name} FULL STOP! Cannot move outside communications range to ${this.direction} from ${this.position.x},${this.position.y}.`;
				//console.log(lastMessage);
				return false;
			}

			// obstacle detection
			const obstacle = obstacles.find(obstacle => obstacle.x === newPosition.x && obstacle.y === newPosition.y);
			if (obstacle !== undefined) {
				obstacle.found = true; // mark obstacle as found, so it will be drawn
				lastMessage = `${this.name} FULL STOP! Our sensors are detecting a Martian ahead at ${this.direction} in ${newPosition.x},${newPosition.y}.`;
				//console.log(lastMessage);
				return false;
			}

			// detect collision with another rover
			for (const rover in rovers) {
				if (rovers[rover].position.x === newPosition.x && rovers[rover].position.y === newPosition.y) {
					lastMessage = `${this.name} FULL STOP! We don't want to hit ${rovers[rover].name} at ${this.direction} in ${rovers[rover].position.x},${rovers[rover].position.y}.`;
					//console.log(lastMessage);
					return false;
				}
			}

			this.position = {...newPosition}; // update position
			this.log.push({...this.position});
			lastMessage = `${this.name} moving towards ${this.direction} to coordinates ${this.position.x},${this.position.y}.`;
			//console.log(lastMessage);
			return true;
		} // moveForward()


		// set glyph according to current direction
		setGlyph() {
			switch (this.direction) {
				case 'NORTH':
					this.glyph = '▲';
					break;
				case 'EAST':
					this.glyph = '►';
					break;
				case 'SOUTH':
					this.glyph = '▼';
					break;
				case 'WEST':
					this.glyph = '◄';
			}
		}


		turnLeft() {
			switch (this.direction) {
				case 'NORTH':
					this.direction = 'WEST';
					break;
				case 'EAST':
					this.direction = 'NORTH';
					break;
				case 'SOUTH':
					this.direction = 'EAST';
					break;
				case 'WEST':
					this.direction = 'SOUTH';
			}
			if (this.userControlled === true) this.setGlyph();
			lastMessage = `${this.name} turning left to face ${this.direction}.`;
			//console.log(lastMessage);
		}


		turnRight() {
			switch (this.direction) {
				case 'NORTH':
					this.direction = 'EAST';
					break;
				case 'EAST':
					this.direction = 'SOUTH';
					break;
				case 'SOUTH':
					this.direction = 'WEST';
					break;
				case 'WEST':
					this.direction = 'NORTH';
			}
			if (this.userControlled === true) this.setGlyph();
			lastMessage = `${this.name} turning right to face ${this.direction}.`;
			//console.log(lastMessage);
		}

	} // Rover class


	// returns an array of n items with unique x,y positions of coordinates between 0 and 9
	// I first tried using a new Set(), but it couldn't differentiate arrays or objects
	function generateUniquePositions(n: number): IPosition[] {
		const ran10 = () => Math.floor(Math.random()*10) as RoverRange;
		const positions: any = []; // how to type empty collection as IPosition[] ?
		while (positions.length < n) {
			const newPos: IPosition = {x:ran10(), y:ran10()};
			if (positions.some((pos: IPosition) => pos.x === newPos.x && pos.y === newPos.y) === false)
				positions.push(newPos);
		}
		return positions;
	}


	function printMap() {
		let mapLines = 'Remote Control a Mars Rover\nUse arrow keys to control, esc to exit\nMap inside the communications range of the Mars lander:\n';

		for (let y=0; y<=9; ++y) {
			let line: string = '';
			for (let x=0; x<=9; ++x) {

				let posDrawn: Boolean = false;

				const obstacle = obstacles.some(obstacle => obstacle.x === x && obstacle.y === y && obstacle.found === true);
				if (obstacle === true) { // draw found obstacles
					line += '@ '; // Martian
					posDrawn = true;
				}
				else
				for (const rover in rovers) { // draw rover position with its starting letter
					const r = rovers[rover];
					if (r.position.x === x && r.position.y === y) {
						if (!posDrawn) {
							line += `${r.glyph} `;
							posDrawn = true;
						}
					}
				}

				if (!posDrawn) line += '- '; // draw empty grid
			}
			mapLines += `${line}\n`;
		}

		console.clear();
		console.log(`${mapLines}\n${lastMessage}`);
	}


	// delay the step function execution
	function setDelay(rover: NamedRover) {
		const delay = Math.floor(Math.random()*1001+1000); // somewhere between 1 and 2 seconds (try 100ms for more speed)
		setTimeout(step, delay, rover);
	}


	// start taking steps with a rover
	function step(rover: NamedRover) {
		printMap();
		if (rover.lastMoveSuccessful === true) {
			rover.lastMoveSuccessful = rover.moveForward();
		}
		else { // random turn direction
			const turnCommands = ['left', 'right'];
			const turnCommand = turnCommands[Math.floor(Math.random() * turnCommands.length)];
			turnCommand === 'left' ? rover.turnLeft() : rover.turnRight();
			rover.lastMoveSuccessful = true;
		}
		setDelay(rover);
	}


	interface IRovers {
		[x: string]: NamedRover; // allow any number of NamedRover objects inside
	}

	// randomize rover and obstacle positions
	const positions = generateUniquePositions(6);
	const rover1 = new NamedRover('Sojourner', positions.pop() as IPosition);
	const rover2 = new NamedRover('Opportunity', positions.pop() as IPosition);
	const myRover = new NamedRover('RemoteRover', positions.pop() as IPosition, true); // user controllable rover
	const rovers: IRovers = {Sojourner:rover1, Opportunity:rover2, RemoteControl:myRover};
	const obstacles: IOposition[] = [ // list of obstacle positions
		{...positions.pop() as IPosition, found:false},
		{...positions.pop() as IPosition, found:false},
		{...positions.pop() as IPosition, found:false},
	];
	let lastMessage = '';
	let actionCount = 0;


	printMap();
	// random initial delays
	setDelay(rover1);
	setDelay(rover2);


process.stdin.setRawMode(true); // read every keypress
process.stdin.on('data', buf => {
	switch (getKey(buf)) {
		case 'up':
		case 'up2':
			myRover.moveForward();
			++actionCount;
			printMap();
			break;
		case 'down':
		case 'down2':
			myRover.moveBackward();
			++actionCount;
			printMap();
			break;
		case 'right':
		case 'right2':
			myRover.turnRight();
			++actionCount;
			printMap();
			break;
		case 'left':
		case 'left2':
			myRover.turnLeft()
			++actionCount;
			printMap();
			break;
		case 'esc':
		case 'ctrl_c':
			const foundObstacles = obstacles.reduce((acc, cur)=> {
				if (cur.found === true) return acc + 1;
				else return acc;
			}, 0); // initial acc
		
			lastMessage = `You made ${actionCount} action${actionCount!==1 ? 's' : ''} and found ${foundObstacles} out of ${obstacles.length} Martians.\n`;
			printMap();
			process.exit();
			break;
	}
});


interface IKeyCodes {
	[x: string]: Buffer; // allow any number of Buffer objects inside
}

const keyCodes: IKeyCodes = {
	ctrl_c: Buffer.from([0x03]), // SIGINT
	esc:    Buffer.from([0x1b]),
	up:     Buffer.from([0x1b, 0x5b, 0x41]),
	up2:    Buffer.from([0x1b, 0x4f, 0x41]), // alternative arrow code
	down:   Buffer.from([0x1b, 0x5b, 0x42]),
	down2:  Buffer.from([0x1b, 0x4f, 0x42]),
	right:  Buffer.from([0x1b, 0x5b, 0x43]),
	right2: Buffer.from([0x1b, 0x4f, 0x43]),
	left:   Buffer.from([0x1b, 0x5b, 0x44]),
	left2:  Buffer.from([0x1b, 0x4f, 0x44]),
};

// returns pressed key as a string or undefined if pressed key is not in keyCodes
function getKey(buf: Buffer): string|undefined {
	for (const key in keyCodes) {
		if ( buf.equals(keyCodes[key]) ) return key;
	}
}

} // namespace
