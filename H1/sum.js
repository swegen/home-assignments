// H1.4 Sum
// Erkka N
const readline = require("readline-sync");
const numberString = readline.question("Input numbers separated by spaces: ");

const numberArray = numberString.split(/ +/);

const sum = numberArray.reduce((a, b) => parseInt(a) + parseInt(b));

console.log('The sum is', sum);
