// H1.1 Smallest and largest
// Erkka N
const readline = require("readline-sync");
const numberString = readline.question("Input numbers separated by spaces: ");

const numberArray = numberString.split(' ');

console.log('Smallest number is', Math.min(...numberArray));
console.log('Largest number  is', Math.max(...numberArray));
