// H1.2 Number between
// Erkka N
const readline = require("readline-sync");
const number = readline.question("Input number between 0 and 10: ");

if (number >= 0 && number <= 10)
    console.log(number);
else
    console.error('Error: Number is not between 0 and 10.');
