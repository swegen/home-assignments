// H2.1 Bulls and Cows
// Erkka N
import rl from "readline-sync";
console.log('Bulls and Cows');

const words: string[] = ["acid","beta","cake","diet","evil","film","gore","heat","iron","jury","knot","lawn","moat","node","oral","pawn","quit","ring","send","thug","unix","vote","weak","xmas","yoga","zero"];
const word: string = words[Math.floor(Math.random() * words.length)].toUpperCase();

function check(guess: string) {
	const guess_array = guess.split('');
	const word_array = word.split('');
	let bulls = 0;
	let cows = 0;

	// find bulls first
	guess_array.forEach((guess_char, guess_index) => {
		if (guess_char === word_array[guess_index]) {
			word_array[guess_index] = ''; // remove found char from word_array
			guess_array[guess_index] = ' '; // blank the found char from guess_array, so the indices differ
			++bulls;
		}
	});

	// then check if there are any cows remaining
	guess_array.forEach((guess_char) => {
		const index = word_array.indexOf(guess_char);
		if (index >= 0) {
			word_array[index] = ''; // remove found char, so it's not found again on the next iteration
			++cows;
		}
	});

	return {bulls:bulls, cows:cows};
}

while (true) {
	const guess: string = rl.question(`Guess a ${word.length} letter word: `).toUpperCase();
	const {bulls, cows} = check(guess);
	console.log(`${bulls} Bull${bulls!==1 ? 's' : ''}, ${cows} Cow${cows!==1 ? 's' : ''}`);
	if (bulls === word.length) break;
}
