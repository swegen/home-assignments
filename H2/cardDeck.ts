// H2.2 Deck of cards
// Erkka N
console.log('Deck of cards');

const deck: string[] = [
	'A♣','2♣','3♣','4♣','5♣','6♣','7♣','8♣','9♣','10♣','J♣','Q♣','K♣',
	'A♦','2♦','3♦','4♦','5♦','6♦','7♦','8♦','9♦','10♦','J♦','Q♦','K♦',
	'A♥','2♥','3♥','4♥','5♥','6♥','7♥','8♥','9♥','10♥','J♥','Q♥','K♥',
	'A♠','2♠','3♠','4♠','5♠','6♠','7♠','8♠','9♠','10♠','J♠','Q♠','K♠',
];

// swap all array items to random indices, works for any array type
// modifies array in place
function shuffle(array: any[]) {
	for (let i: number = 0; i<array.length; ++i) {
		const randomIndex: number = Math.floor(Math.random() * (array.length));
		const swappedValue: any = array[i]; // save current index
		array[i] = array[randomIndex]; // make it random
		array[randomIndex] = swappedValue; // restore current index to random position
	}
}

// takes the top card off the deck and returns it
function takeTopCard(deck: string[]) {
	if (deck.length > 0) return deck.shift();
}

shuffle(deck);
console.log('The first card out the deck is', takeTopCard(deck));
