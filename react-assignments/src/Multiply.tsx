// 6b - React basics - Assignment 2: Reusable component
// Erkka N

import React from 'react';

// React Functional Component
interface IProps {firstValue: number, secondValue: number}
const Multiplier: React.FC<IProps> = props => {
		const { firstValue, secondValue } = props;
    const multiplyValues = () => firstValue * secondValue;
    return (
        <div>
            <p>
                {firstValue} times {secondValue} is <b>{multiplyValues()}</b> which is {multiplyValues() > 100 ? 'Greater than' : 'Less or equal to'} a hundred.
            </p>
        </div>
    );
};

export const Multiply = () => (
    <div>
      <h2>Multiplication</h2>
      <Multiplier firstValue={2} secondValue={4}/>
      <Multiplier firstValue={4} secondValue={8}/>
      <Multiplier firstValue={8} secondValue={16}/>
      <Multiplier firstValue={16} secondValue={32}/>
    </div>
);
  