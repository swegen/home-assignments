// H6 Note-taking CRUD application
// Erkka N

import React, { useRef, useState } from 'react'
import './Notes.css';


interface IArrayNote {
	date: Date
	text: string
}

interface IReactNote {
	id: number
	note: IArrayNote
	deleteFromNotes: Function
	saveNotes: Function
}


const Note: React.FC<IReactNote> = props => { // React Stateful Functional Component
	const { id, note, deleteFromNotes, saveNotes } = props;

	const textArea = useRef<HTMLTextAreaElement>(null); // textArea.current contains reference to textArea element
	function getTextAreaValue() {
		if (textArea.current !== null) return textArea.current.value.trim();
		else return '';
	};

	const [edit, setEdit] = useState<boolean>(note.text === ''); // show edit dialog when adding new note
	const [saveDisabled, setSaveDisabled] = useState<boolean>(note.text === ''); // save button state

	const deleteNote = () => deleteFromNotes(id); // calls function from parent
	const editNote = () => setEdit(true);
	const textChange = () => {
		if (getTextAreaValue() === '') setSaveDisabled(true);
		else if (saveDisabled) setSaveDisabled(false);
	};	
	const cancelEdit = () => {
		if (note.text === '') deleteNote();
		else setEdit(false);
	};
	const saveNote = () => {
		const edited = getTextAreaValue();
		if (edited !== '') {
			note.text = edited;
			saveNotes();
			setEdit(false);
		}
	};

	return (
		<li>
			<time dateTime={note.date.toISOString()}>{note.date.toLocaleString()}</time>
			{!edit &&
			<div className="note">
				<pre>{note.text}</pre>
				<div className="buttons">
					<button onClick={deleteNote}>Delete</button>
					<button onClick={editNote}>Edit</button>
				</div>
			</div>
			}
			{edit &&
			<div className="edit">
				<textarea autoFocus ref={textArea} onChange={textChange} defaultValue={note.text}></textarea>
				<div className="buttons">
					<button onClick={cancelEdit}>Cancel</button>
					<button onClick={saveNote} disabled={saveDisabled}>Save</button>
				</div>
			</div>
			}
		</li>);
}; // Note: React.FC<IReactNote>


// React Stateful Functional Component
export const Notes: React.FC = () => {

	// load / save notes to localStorage
	const localStorageKey = 'noteZ';
	function loadNotes() {
		const notes = localStorage.getItem(localStorageKey);
		if (notes === null) return [];
		else return JSON.parse( notes,  (key, value) => key === 'date' ? new Date(value) : value ); // revive Date-objects
	}
	const [notes, setNotes] = useState<IArrayNote[]>(loadNotes());

	const saveNotes = (data: any) => {
		if (data === undefined) data = notes;
		localStorage.setItem(localStorageKey, JSON.stringify(data));
	};

	const addNote = () => {
		if (notes.length === 0 || notes[notes.length-1].text !== '') // add new note only after the empty one is saved/cancelled
			setNotes([...notes, {date:new Date(), text:''}]);
	};

	const deleteNote = (id: number) => {
		const filteredNotes = notes.filter((note, idx) => idx !== id);
		setNotes(filteredNotes); // async? does not change notes right away
		saveNotes(filteredNotes); // save to localStorage
	};

	return (
		<div id="notes">
			<h2>Notes</h2>
			<button onClick={addNote}>Add</button>
			<ul>{notes.map((note, idx) => (<Note key={idx} id={idx} note={note} deleteFromNotes={deleteNote} saveNotes={saveNotes}/>))}</ul>
		</div>
	);
};
