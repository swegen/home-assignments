import React, { useEffect, useState } from 'react'

export const Efekti = () => {

	const [data, setData] = useState('');
	const [postId, setPostId] = useState<number>(1);

	useEffect(() => {
		(async () => {
			const res = await fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`);
			const data = await res.text();
			setData(data);
		})();
	}, [postId]);

	return (
	<div>
		<div><button onClick={()=>setPostId(postId+1)}>Fetch next post</button></div>
		{data}
	</div>
	);
};
