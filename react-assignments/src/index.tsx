// 7 - React advanced
// Erkka N

import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, NavLink, Route } from 'react-router-dom'
import './index.css';
import { Hello } from './Hello'
import { Multiply } from './Multiply'
import { Bingo } from './Bingo'
import { Notes } from './Notes'
import { Modaali } from './Modaali';
import { Efekti } from './Efekti';
import { MemoryGame } from './MemoryGame';

function router(path: string) {
	switch (path) {
		case '/notes':    return <Notes/>;
		case '/bingo':    return <Bingo/>;
		case '/multiply': return <Multiply/>;
		case '/hello':    return <Hello/>;
		default: return <p>Select a React component from the list above.</p>;
	}
}

const activeStyle = { color: 'red' };

ReactDOM.render(
	<React.StrictMode>
		<BrowserRouter>
			<nav>
				<NavLink exact to="/" activeStyle={activeStyle}>H7 Memory game</NavLink>
				<NavLink to="/modals" activeStyle={activeStyle}>react-modal</NavLink>
				<NavLink to="/effects" activeStyle={activeStyle}>useEffect</NavLink>
				<NavLink to="/H6" activeStyle={activeStyle}>H6 exercises</NavLink>
			</nav>
			<Route exact path="/"><MemoryGame/></Route>
			<Route path="/modals">
				<Modaali title="Food" message="Eat health food!" color="lightpink"/>
				<Modaali title="Water" message="Drink lots of water!1" color="yellow"/>
			</Route>
			<Route path="/effects"><Efekti/></Route>
			<Route path="/H6">
				<h1>React assignments</h1>
				<nav>
					<a href="/notes">H6 Notes</a>
					<a href="/bingo">Bingo</a>
					<a href="/multiply">Multiply</a>
					<a href="/hello">Hello</a>
				</nav>
				{router(window.location.pathname)}
			</Route>
		</BrowserRouter>
	</React.StrictMode>
	,document.getElementById('root')
	//,document.getElementsByTagName('body')[0]
);
