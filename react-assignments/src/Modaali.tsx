import React from 'react'
import Modal from 'react-modal';


const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
		transform             : 'translate(-50%, -50%)',
		background: '#222',
  }
};


Modal.setAppElement('#root');
if (Modal.defaultStyles.overlay) Modal.defaultStyles.overlay.backgroundColor = 'rgba(0,0,0,0.6)';

// React Stateful Functional Component
interface IModaali { title:string, message:string, color:string }
export const Modaali: React.FC<IModaali> = props => {
	const { title, message, color } = props;
	const [modalIsOpen,setIsOpen] = React.useState(false);
  function openModal() {
    setIsOpen(true);
  }

  function closeModal() {
    setIsOpen(false);
  }

    return (
      <div>
        <button onClick={openModal}>{title}</button>
        <Modal
          isOpen={modalIsOpen}
          onRequestClose={closeModal}
          style={customStyles}
          contentLabel={title}
        >
          <div style={{color:color}}>{message}</div>
          <button onClick={closeModal}>Close</button>
        </Modal>
      </div>
    );
};
