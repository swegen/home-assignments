import React, { useEffect, useState } from 'react'
import './MemoryGame.css';


const gridStyles: React.CSSProperties = {
	display: 'grid',
	gridTemplateColumns: 'repeat(4, 1fr)',
	columnGap: '10px',
	rowGap: '1em',
	WebkitTapHighlightColor: 'transparent',
	MozUserSelect: 'none',
	WebkitUserSelect: 'none',
	msUserSelect: 'none',
	userSelect: 'none',
};


// {flipped ? props.symbol : '❓'}
interface IMemoryCard { symbol: string, checkCard: Function }
const MemoryCard: React.FC<IMemoryCard> = props => {
	const { symbol, checkCard } = props;
	const [cardState, setCardState] = useState<'closed'|'opened'|'pair'>('closed');

	function click() {
		if (cardState === 'closed') checkCard({symbol:symbol, setCardState:setCardState});
	}

	return (
		<div onClick={click} className={`card ${cardState}`}>
			<div className="inner">
				<div className="front">❓</div>
				<div className="back">{symbol}</div>
			</div>
		</div>
	);
};


interface ICheckCard { symbol: string, setCardState: Function }
export const MemoryGame = () => {
	const uniqueSymbols = ['⛺', '🍔', '🍓', '⛸', '⛴', '⛱', '⛄', '⚽'];
	const memorySymbols = [...uniqueSymbols, ...uniqueSymbols]; // duplicate symbols

	const shuffledCards = () => 
		shuffle(memorySymbols).map((symbol, idx) => 
			<MemoryCard key={idx} symbol={symbol} checkCard={checkCards}/>);
	const [cards, setCards] = useState(shuffledCards);

	// couldn't get this to update with useState, so I worked around that with setStatus
	let pairsTurned: number = 0;

	const [status, setStatus] = useState(`${pairsTurned} pairs turned.`);

	const flippedCards: ICheckCard[] = [];
	function checkCards(card:ICheckCard) {
		if (flippedCards.length < 2) {
			flippedCards.push(card);
			card.setCardState('opened'); // set child's state
		}
		if (flippedCards.length >= 2) {
			setStatus(`${++pairsTurned} pairs turned.`);
			const cards = [flippedCards.pop() as ICheckCard, flippedCards.pop() as ICheckCard];
			if (cards[0].symbol === cards[1].symbol) {
				cards[0].setCardState('pair');
				cards[1].setCardState('pair');
			} else {
				setTimeout(() => {
					cards[0].setCardState('closed');
					cards[1].setCardState('closed');
				}, 1500);
			}
		}
	}

	/*
	useEffect(() => {
		(async () => {
			const res = await fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`);
			const data = await res.text();
			setData(data);
		})();
	}, [postId]);
*/

	return (
		<div>
			<h1>Memory game</h1>
			<div>{status}</div>
			<div style={gridStyles}>
				{cards}
			</div>
		</div>
	);
};


// swap all array items to random indices, works for any array type
// modifies array in place
function shuffle(array: any[]) {
	for (let i: number = 0; i<array.length; ++i) {
		const randomIndex: number = Math.floor(Math.random() * (array.length));
		const swappedValue: any = array[i]; // save current index
		array[i] = array[randomIndex]; // make it random
		array[randomIndex] = swappedValue; // restore current index to random position
	}
	return array;
}
