// 6b - React basics - Assignment 3: React Stateful Functional Component
// Erkka N

import React, { useState } from 'react'

// https://en.wikipedia.org/wiki/Bingo_(American_version)
const numberOfBalls = 75;

const styles = `
button,input{font:inherit}

#bingo{line-height: 1}
#bingo button{
  background: gold;
  cursor: pointer;
  box-shadow: 1px 2px 3px #888;
  margin-right: .5em;
}
#bingo ul {padding: 0}
#bingo li {
  font-weight: bold;
  list-style-type: none;
  display: inline-block;
  background: orange;
  margin: .5em;
  padding: .5em;
  border-radius: 50%;
  width: 2em;
  height: 2em;
  text-align: center;
  color: #111;
  text-shadow: 1px 2px 3px #ff0;
  box-shadow: 1px 2px 3px #888;
  white-space: nowrap;
  overflow: hidden;
}
`;

// swap all array items to random indices, works for any array type (modifies array in place)
function shuffle(array: any[]) {
	for (let i: number = 0; i<array.length; ++i) {
		const randomIndex: number = Math.floor(Math.random() * (array.length));
		const swappedValue: any = array[i]; // save current index
		array[i] = array[randomIndex]; // make it random
		array[randomIndex] = swappedValue; // restore current index to random position
	}
}

// returns an array with n shuffled bingo balls
function genBalls(n: number) {
	const balls = [];
	do {
		let letter;
		if (n > 60) letter = 'O';
		else
		if (n > 45) letter = 'G';
		else
		if (n > 30) letter = 'N';
		else
		if (n > 15) letter = 'I';
		else letter = 'B';
		balls.push({letter, number:n});
	} while (--n)
	shuffle(balls);
	return balls;
}

let ballBasket = genBalls(numberOfBalls);
let disabledButton = false;


// React Functional Component
interface IBingoBall { letter:string, number:number }
const BingoBall: React.FC<IBingoBall> = props => {
	return (<li>{props.letter}<br/>{props.number}</li>);
};

// React Stateful Functional Component
export const Bingo: React.FC = () => {
	const [takenBalls, setTakenBalls] = useState<IBingoBall[]>([]);

	const addBall = () => {
		const newBall = ballBasket.pop();
		if (ballBasket.length === 0) disabledButton = true;
		if (newBall !== undefined) setTakenBalls([...takenBalls, newBall]);
	};

	const reset = () => {
		ballBasket = genBalls(numberOfBalls);
		disabledButton = false;
		setTakenBalls([]);
	};

	return (
		<div id="bingo">
			<style>{styles}</style>
			<h2>B I N G O</h2>
			<button disabled={disabledButton} onClick={addBall}>+</button>{takenBalls.length > 0 && <button onClick={reset}>R</button>} Ball {takenBalls.length} / {ballBasket.length}
			<ul>{takenBalls.map(ball => (<BingoBall key={ball.number} letter={ball.letter} number={ball.number}/>))}</ul>
		</div>
	);
};
