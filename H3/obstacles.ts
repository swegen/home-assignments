// H3.6 Obstacles
// Erkka N
{ // blank namespace to prevent name clashes

	type RoverRange = 0|1|2|3|4|5|6|7|8|9; // there seems to be no general range type?
	
	interface IPosition {
		x: RoverRange;
		y: RoverRange;
	}
	
	interface IOposition extends IPosition {
		found: Boolean
	}

	const obstacles: IOposition[] = [{x:3, y:3, found:false}, {x:7, y:0, found:false}, {x:8, y:8, found:false}]; // list of obstacle positions
	
	class NamedRover {
		public position: IPosition;
		private direction: 'NORTH' | 'EAST' | 'SOUTH' | 'WEST';
		public log: IPosition[];
		private name: string;
	
	
		constructor(name: string, position: IPosition) {
			this.name = name;
			this.position = position;
			this.direction = 'SOUTH';
			this.log = [{...this.position}]; // make a copy of current position object
		}
	
	
		moveForward() {
			const newPosition: IPosition = {...this.position};

			switch (this.direction) {
				case 'NORTH':
					--newPosition.y;
					break;
				case 'EAST':
					++newPosition.x;
					break;
				case 'SOUTH':
					++newPosition.y;
					break;
				case 'WEST':
					--newPosition.x;
					break;
			}
	
			// check to stay on the grid
			if (newPosition.x < 0 || newPosition.x > 9 || newPosition.y < 0 || newPosition.y > 9) {
				console.log(`${this.name} FULL STOP! Cannot move outside communications range.`);
				return;
			}

			// obstacle detection
			const obstacle = obstacles.find(obstacle => newPosition.x === obstacle.x && newPosition.y === obstacle.y);
			if (obstacle !== undefined) {
				obstacle.found = true;
				console.log(`${this.name} FULL STOP! Our sensors are detecting a Martian ahead?!`);
				return;
			}

			// detect collision with another rover
			for (const rover in rovers) {
				if (rovers[rover].position.x === newPosition.x && rovers[rover].position.y === newPosition.y) {
					console.log(`${this.name} FULL STOP! We don't want to hit ${rovers[rover].name} at ${rovers[rover].position.x},${rovers[rover].position.y}.`);
					return;
				}
			}

			this.position = {...newPosition}; // update position
			this.log.push({...this.position});
			console.log(`${this.name} moving towards ${this.direction} to coordinates ${this.position.x},${this.position.y}.`);
		}
	
	
		turnLeft() {
			switch (this.direction) {
				case 'NORTH':
					this.direction = 'WEST';
					break;
				case 'EAST':
					this.direction = 'NORTH';
					break;
				case 'SOUTH':
					this.direction = 'EAST';
					break;
				case 'WEST':
					this.direction = 'SOUTH';
					break;
			}
			console.log(`${this.name} turning left to face ${this.direction}.`);
		}
	
		turnRight() {
			switch (this.direction) {
				case 'NORTH':
					this.direction = 'EAST';
					break;
				case 'EAST':
					this.direction = 'SOUTH';
					break;
				case 'SOUTH':
					this.direction = 'WEST';
					break;
				case 'WEST':
					this.direction = 'NORTH';
					break;
			}
			console.log(`${this.name} turning right to face ${this.direction}.`);
		}
	
		printPosition() {
			console.log(`${this.name} currently positioned in ${this.position.x},${this.position.y}.`)
		}
	
		// prints travelled path for all rovers in the vicinity
		printLog() {
			console.log(`Travel Log:`);
	
			for (let y=0; y<=9; ++y) {
				let line: string = '';
				for (let x=0; x<=9; ++x) {
	
					let posDrawn: Boolean = false;

					const obstacle = obstacles.some(obstacle => x === obstacle.x && y === obstacle.y && obstacle.found === true);
					if (obstacle === true) { // draw possible obstacle
						line += '@ '; // Martian
						posDrawn = true;
					}
					else
					for (const rover in rovers) { // draw rover position
						const r = rovers[rover];
						r.log.forEach((pos, i) => {
							if (pos.x === x && pos.y === y) {
								if (!posDrawn) {
									const char = (i===0 ? r.name[0].toUpperCase() : r.name[0].toLowerCase()); // starting position as uppercase letter
									line += `${char} `;
									posDrawn = true;
								}
							}
						});			
					}

					if (!posDrawn) line += '- '; // draw empty grid
				}
				console.log(line);
			}
	
		}
	} // Rover class
	
	
	interface IRovers {
		[x: string]: NamedRover; // allow any number of NamedRover objects inside
	}
	
	const rover1 = new NamedRover('Sojourner', {x:3, y:2});
	const rover2 = new NamedRover('Opportunity', {x:7, y:7});
	const rovers: IRovers = {Sojourner:rover1, Opportunity:rover2};
	
	rover1.moveForward();
	rover1.turnLeft();
	rover1.moveForward();
	rover1.moveForward();
	rover1.turnRight();
	rover1.moveForward();
	rover1.printPosition();
	rover1.printLog();
	
	console.log(); // newline
	
	rover2.moveForward();
	rover2.turnRight();
	rover2.moveForward();
	rover2.turnLeft();
	rover2.moveForward();
	rover2.moveForward(); // try to go off the grid
	rover2.printPosition();
	rover2.printLog();
	
	} // namespace
	