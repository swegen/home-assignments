// H3.1 Mars Rover
// Erkka N

const rover = {
	position: {x:5, y:0},
	direction: 'SOUTH',
	log: [{x:5, y:0}], // how can this be initialized to position without redefining?
	
	moveForward() {
		switch (this.direction) {
			case 'NORTH':
				--this.position.y;
				break;
			case 'EAST':
				++this.position.x;
				break;
			case 'SOUTH':
				++this.position.y;
				break;
			case 'WEST':
				--this.position.x;
				break;
		}
		this.log.push({...this.position});
		console.log(`Moving towards ${this.direction} to coordinates ${this.position.x},${this.position.y}.`)
	},

	turnLeft() {
		switch (this.direction) {
			case 'NORTH':
				this.direction = 'WEST';
				break;
			case 'EAST':
				this.direction = 'NORTH';
				break;
			case 'SOUTH':
				this.direction = 'EAST';
				break;
			case 'WEST':
				this.direction = 'SOUTH';
				break;
		}
		console.log(`Turning left to face ${this.direction}.`);
	},

	turnRight() {
		switch (this.direction) {
			case 'NORTH':
				this.direction = 'EAST';
				break;
			case 'EAST':
				this.direction = 'SOUTH';
				break;
			case 'SOUTH':
				this.direction = 'WEST';
				break;
			case 'WEST':
				this.direction = 'NORTH';
				break;
		}
		console.log(`Turning right to face ${this.direction}.`);
	},

	printPosition() {
		console.log(`Currently positioned in ${this.position.x},${this.position.y}.`)
	},

	printLog() {
		console.log('Travel Log:');
		this.log.forEach(pos => {
			console.log(`    ${pos.x},${pos.y}`);
		});
	}
}; // rover object

rover.moveForward();
rover.turnRight();
rover.moveForward();
rover.turnLeft();
rover.moveForward();
rover.printPosition();
rover.printLog();
