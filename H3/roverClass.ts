// H3.2 Rover class
// Erkka N

interface Pos {
	x: number;
	y: number;
}


class Rover {
	private position: Pos;
	private direction: 'NORTH' | 'EAST' | 'SOUTH' | 'WEST';
	private log: Pos[];

	constructor() {
		this.position = {x:5, y:0};
		this.direction = 'SOUTH';
		this.log = [{...this.position}]; // make a copy of current position object
	}

	moveForward() {
		switch (this.direction) {
			case 'NORTH':
				--this.position.y;
				break;
			case 'EAST':
				++this.position.x;
				break;
			case 'SOUTH':
				++this.position.y;
				break;
			case 'WEST':
				--this.position.x;
				break;
		}
		this.log.push({...this.position});
		console.log(`Moving towards ${this.direction} to coordinates ${this.position.x},${this.position.y}.`)
	}

	turnLeft() {
		switch (this.direction) {
			case 'NORTH':
				this.direction = 'WEST';
				break;
			case 'EAST':
				this.direction = 'NORTH';
				break;
			case 'SOUTH':
				this.direction = 'EAST';
				break;
			case 'WEST':
				this.direction = 'SOUTH';
				break;
		}
		console.log(`Turning left to face ${this.direction}.`);
	}

	turnRight() {
		switch (this.direction) {
			case 'NORTH':
				this.direction = 'EAST';
				break;
			case 'EAST':
				this.direction = 'SOUTH';
				break;
			case 'SOUTH':
				this.direction = 'WEST';
				break;
			case 'WEST':
				this.direction = 'NORTH';
				break;
		}
		console.log(`Turning right to face ${this.direction}.`);
	}

	printPosition() {
		console.log(`Currently positioned in ${this.position.x},${this.position.y}.`)
	}

	printLog() {
		console.log('Travel Log:');
		this.log.forEach(pos => {
			console.log(`    ${pos.x},${pos.y}`);
		});
	}
} // Rover class


const marsRover = new Rover();
marsRover.moveForward();
marsRover.turnRight();
marsRover.moveForward();
marsRover.turnLeft();
marsRover.moveForward();
marsRover.printPosition();
marsRover.printLog();
