import crypto from 'crypto';

// a native password hashing function (secret) => {hash, salt}
export function hash(secret: string) {
	const salt = crypto.randomBytes(16).toString('utf16le'); // ~24 bytes string
	const hash = crypto.scryptSync(secret, salt, 64).toString('utf16le');
	return {hash:hash, salt:salt};
}
export const verifyHash = (secret: string, salt: string) => crypto.scryptSync(secret, salt, 64).toString('utf16le');

// JSON.stringify with pretty indentation output
export const toJson = (data: any) => JSON.stringify(data, null, 2);
