import fs from 'fs';
import { toJson } from './utils';

const userDB = 'userDB.json';

export interface IUser {
	name: string;
	password_hash: string;
	password_salt: string;
	borrowed_books: { isbn:string, titleAuthorYear:string, due_date:Date, copy_id:number }[];
  books_history: { isbn:string, titleAuthorYear:string, date_returned:Date }[]; // history of returned books
}

interface IUsers {
	[x: number]: IUser; // allow any number of IUser objects inside
	lastId: number; // unique id
	save: Function;
}

// restore users object from file if exists, or create an empty object, add save function
export const users: IUsers = (() => {
	try { return JSON.parse(fs.readFileSync(userDB, 'utf8')) }
	catch { return {lastId: 0} }
})();

users.save = () => fs.writeFileSync(userDB, toJson(users));
