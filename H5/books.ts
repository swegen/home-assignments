import fs from 'fs';
import { toJson } from './utils';

const bookDB = 'bookDB.json';

// a book object includes an array of copies. this is its typing
interface ICopy {
	id: string;
	status: 'borrowed' | 'in_library' | 'not_available';
	borrower_id: number | null;
	due_date: Date | null;
}

// restore books object from file if exists, or create an empty object, add save, borrow and return functions
export const books = (() => { // named export
	try { return JSON.parse(fs.readFileSync(bookDB, 'utf8')) }
	catch { return [] }
})();

books.save = () => fs.writeFileSync(bookDB, toJson(books));

// returns a formatted string containing book title, author and year
books.getTitleAuthorYear = (book: any): string => {
	const year = new Date(book.published).getFullYear();
	return `${book.title} by ${book.author} (${year})`;
};

// sets a copy as borrowed. returns book if success, undefined if failure. saves the db.
books.borrow = (isbn: string, borrower_id: number) => {
	const book = books.find((book: any) => book.isbn === isbn); // TODO: book interface
	if (book === undefined) return undefined; // no book found with given isbn

	const availableCopy: ICopy = book.copies.find((copy: ICopy) => copy.status === 'in_library');
	if (availableCopy === undefined) return undefined; // no copies are available
	else {
		availableCopy.borrower_id = borrower_id;
		availableCopy.status = 'borrowed';
		const due = new Date();
		due.setDate(due.getDate() + 28); // four weeks from now :D
		availableCopy.due_date = due;
		books.save();
		return {isbn:book.isbn, titleAuthorYear:books.getTitleAuthorYear(book), due_date:availableCopy.due_date, copy_id:availableCopy.id};
	}
};

// sets a copy status to 'in_library'. returns book if success, undefined if failure. saves the db.
books.return = (borrowedBook: any, borrower_id: number) => {
	const book = books.find((book: any) => book.isbn === borrowedBook.isbn);
	if (book === undefined) return undefined; // no book found with given isbn

	const borrowedCopy = book.copies.find((copy: ICopy) => copy.id === borrowedBook.copy_id && copy.status === 'borrowed' && copy.borrower_id === borrower_id);
	if (borrowedCopy === undefined) return undefined; // the copy of the book is not currently borrowed by the borrower
	else {
		borrowedCopy.borrower_id = null;
		borrowedCopy.status = 'in_library';
		borrowedCopy.due_date = null;
		books.save();
		return {isbn:borrowedBook.isbn, titleAuthorYear:borrowedBook.titleAuthorYear, date_returned:new Date()};
	}
};
