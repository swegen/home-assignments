// H5 API for the Library
// Erkka N

import express from 'express';
import bodyParser from 'body-parser';

import { hash, verifyHash, toJson } from './utils';
import { IUser, users } from './users';
import { books } from './books';

const app = express();
const PORT = 5000;


// body-parser middleware
app.use(bodyParser.json());
//app.use(bodyParser.urlencoded({ extended: false }));


// logging middleware
app.use((req, res, next) => {
	console.log(req.method, req.url, req.body);
	next();
});


// send html for testing the library api
app.get('/', (req, res) => {
	res.sendFile(__dirname + '/apitest.html');
});


// I added a password_verify field here, so the user is required to input the same password twice when creating the account
// H5.1 Create a new user with POST (name, password, password_verify) => user_id
app.post('/library/new_user', (req, res) => {
	if (req.body.name !== undefined  &&  req.body.name.trim() !== ''  &&  req.body.password !== undefined  &&  req.body.password.trim().length > 1  &&  req.body.password === req.body.password_verify) {
		const user = {} as IUser;
		user.name = req.body.name;
		user.borrowed_books = [];
		user.books_history = [];

		const pass = hash(req.body.password);
		user.password_hash = pass.hash;
		user.password_salt = pass.salt;

		const user_id = ++users.lastId;
		users[user_id] = user; // add user to users object using id as key
		users.save();
		res.send( toJson({user_id:user_id}) ); // send created user_id back
	}
	else res.send( toJson({error:'Invalid given userdata.'}) );
});


// H5.2 GET number of borrowed books (user_id) => number_of_borrowed_books
app.get('/library/:user_id/books_n', (req, res) => {
	const user: IUser = users[Number(req.params.user_id)];
	if (user === undefined) res.send( toJson({error:'Invalid user.'}) ); // res.sendStatus(404);
	else res.send( toJson({books_n:user.borrowed_books.length}) );
});


// H5.3 GET a list of borrowed books (user_id) => borrowed_books[]
app.get('/library/:user_id/books', (req, res) => {
	const user: IUser = users[Number(req.params.user_id)];
	if (user === undefined) res.send( toJson({error:'Invalid user.'}) );
	else res.send( toJson(user.borrowed_books) );
});


// H5.4 Borrow a book with PATCH (user_id, password, isbn) => borrowed_book{}
app.patch('/library/user/borrow', (req, res) => {
	if (req.body.user_id !== undefined  &&  req.body.password !== undefined  &&  req.body.isbn !== undefined) {
		const user: IUser = users[req.body.user_id];
		if (user !== undefined  &&  verifyHash(req.body.password, user.password_salt) === user.password_hash) {
			const borrowed_book = books.borrow(req.body.isbn, req.body.user_id);
			if (borrowed_book !== undefined) { // book is available
				user.borrowed_books.push(borrowed_book);
				users.save();
				res.send(toJson(borrowed_book));
				return; // exit function
			}
		}
	}
	res.send( toJson({error:'Book borrow failed.'}) ); // send errorCode if any of the if checks fail
});


// H5.5 Return a book with PATCH (user_id, password, isbn) => returned_book{}
app.patch('/library/user/return', (req, res) => {
	if (req.body.user_id !== undefined  &&  req.body.password !== undefined  &&  req.body.isbn !== undefined) {
		const user: IUser = users[req.body.user_id];
		if (user !== undefined  &&  verifyHash(req.body.password, user.password_salt) === user.password_hash) {
			const borrowedBookIndex = user.borrowed_books.findIndex(book => book.isbn === req.body.isbn);
			if (borrowedBookIndex !== -1) { // book is found from users borrowed_books list
				const borrowed_book = user.borrowed_books[borrowedBookIndex];
				const returned_book = books.return(borrowed_book, req.body.user_id);
				if (returned_book !== undefined) {
					user.borrowed_books.splice(borrowedBookIndex, 1); // remove from users borrowed_books list
					user.books_history.push(returned_book); // add to users books_history list
					users.save();
					res.send(toJson(returned_book));
					return; // exit function
				}
			}
		}
	}
	res.send( toJson({error:'Book return failed.'}) );
});


// (These H5.6 endpoints should probably validate the password/token too, but this wasn't specified in the assignment)
// H5.6 Update usermame with PATCH (user_id, name) => new_name
app.patch('/library/user/name', (req, res) => {
	if (req.body.user_id !== undefined  &&  req.body.name !== undefined  &&  req.body.name.trim() !== '') {
		const user: IUser = users[req.body.user_id];
		if (user !== undefined) {
			user.name = req.body.name;
			users.save();
			res.send( toJson({new_name:user.name}) );
			return; // exit function
		}
	}
	res.send( toJson({error:'Invalid name!'}) );
});


// H5.6 Update password with PATCH (user_id, password) => new_password
app.patch('/library/user/password', (req, res) => {
	if (req.body.user_id !== undefined  &&  req.body.password !== undefined  &&  req.body.password.trim().length > 1) {
		const user: IUser = users[req.body.user_id];
		if (user !== undefined) {
			const newPass = hash(req.body.password);
			user.password_hash = newPass.hash;
			user.password_salt = newPass.salt;
			users.save();
			res.send( toJson({new_password:req.body.password}) );
			return; // exit function
		}
	}
	res.send( toJson({error:'Invalid password!'}) );
});


app.listen(PORT, () => {
	console.log(`Express server listening on http://localhost:${PORT}`);
});
